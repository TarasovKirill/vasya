﻿using UnityEngine;
using System.Collections;

public class ControllerScript : MonoBehaviour
{
	private float yaw = 0.0f;
	private float pitch = 0.0f;

	public float step = 20.0f;
	public float speed = 20.5f;
	public float runningSpeed = 20.0f;

	#region forRotation
	public float speedH = 2.0f;
	public float speedV = 2.0f;
	#endregion

	#region forWalking
	private bool isRunning = false;
	#endregion

	// Use this for initialization
	void Start ()
	{
		ChangeCursorState.HideCursor ();
	}

	void Update ()
	{	
		LookWithMouse ();
		ReleaseOnEsc ();
		GetBackInOnClick ();
		CheckRunning ();
		MoveOnWasd ();
	}

	void LookWithMouse ()
	{
		yaw += speedH * Input.GetAxis ("Mouse X");
		pitch -= speedV * Input.GetAxis ("Mouse Y");
		pitch = pitch<-90?-90:(pitch>90?90:pitch);
		transform.eulerAngles = new Vector3 (pitch, yaw, 0.0f);
	}

	void ReleaseOnEsc ()
	{
		if (ChangeCursorState.IsLocked () && Input.GetKeyDown (KeyCode.Escape)) {
			ChangeCursorState.ShowCursor ();
		}
	}

	void GetBackInOnClick ()
	{
		if (!ChangeCursorState.IsLocked () && Input.GetMouseButtonDown (0)) {
			ChangeCursorState.HideCursor ();
		}
	}

	//TODO: ended here
	void CheckRunning()
	{
		if (!isRunning && Input.GetKeyDown (KeyCode.LeftShift)) {
			isRunning = true;
		}
		if (isRunning && Input.GetKeyUp (KeyCode.LeftShift)) {
			isRunning = false;
		}
	}
	void MoveOnWasd()
	{ 
		var position = transform.position;
		if (Input.GetKeyDown (KeyCode.W))
			position.x += step;
		if (Input.GetKeyDown (KeyCode.S))
			position.x -= step;
		if (Input.GetKeyDown (KeyCode.A))
			position.y += step;
		if (Input.GetKeyDown (KeyCode.D))
			position.y -= step;
		transform.position = Vector3.MoveTowards (transform.position, position, Time.deltaTime * speed);
	}
}
